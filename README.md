Role Name
=========

A simple role to register RHEL machines to the RHSM.

Requirements
------------

Any RHEL machine can be registered to the RHSM.

Role Variables
--------------

In order for the role to work the following variables sets must be present:
Username and password:
* username: Your rhsm username.
* password: Your rhsm password.
* pool_ids: List of the pool IDs to register against.
  
Activation Key:
* activationkey: Your previously created activationkey.
* org_id: Your organization id.
* pool: List of the pool IDs to register against.

Please note that these sets are mutually exclusive.

Dependencies
------------

N/A

Example Playbook
----------------

This is an example of how to use the role:

```yaml
---
    - name: Prepare machines with RHSM Registration
      ansible.builtin.include_role:
        name: role-rhsm-registration
      when: ansible_os_family == 'RedHat'
      register: rhsm_out
```

Author Information
------------------

Amaya Rosa Gil Pippino
